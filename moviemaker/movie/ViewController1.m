//
//  ViewController1.m
//  movie
//
//  Created by Clicklabs 104 on 10/19/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController1.h"
#import "ViewController.h"
@interface ViewController1 ()
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIButton *video1;
@property (weak, nonatomic) IBOutlet UIButton *video2;
@property (weak, nonatomic) IBOutlet UIButton *video3;
@property (weak, nonatomic) IBOutlet UIButton *video4;
@property (weak, nonatomic) IBOutlet UIButton *video5;
@property (weak, nonatomic) IBOutlet UIImageView *i1;

@property (weak, nonatomic) IBOutlet UIButton *video6;

@property (weak, nonatomic) IBOutlet UIImageView *i2;
@property (weak, nonatomic) IBOutlet UIImageView *i3;
@property (weak, nonatomic) IBOutlet UIImageView *i5;
@property (weak, nonatomic) IBOutlet UIImageView *i6;
@property (weak, nonatomic) IBOutlet UIImageView *demo;
@property (weak, nonatomic) IBOutlet UIImageView *v;
@property (weak, nonatomic) IBOutlet UIImageView *i;
@property (weak, nonatomic) IBOutlet UIImageView *d;
@property (weak, nonatomic) IBOutlet UIImageView *e;
@property (weak, nonatomic) IBOutlet UIImageView *o;

@end

@implementation ViewController1
ViewController *b;
@synthesize demo;
@synthesize imageview;
@synthesize video1;
@synthesize video2;
@synthesize video3;
@synthesize video4;
@synthesize video5;
@synthesize video6;
@synthesize i1;
@synthesize i2;
@synthesize i3;
@synthesize i5;
@synthesize i6;
NSInteger buttontag;
@synthesize v;
@synthesize i;
@synthesize d;
@synthesize e;
@synthesize o;


- (void)viewDidLoad {
    [super viewDidLoad];
    i5.layer.cornerRadius=51/2;
    i5.layer.masksToBounds=YES;
    //i1.layer.borderWidth=1;
    
    demo.layer.cornerRadius=50/2;
    demo.layer.masksToBounds=YES;
    
    i1.layer.cornerRadius=50/2;
    i1.layer.masksToBounds = YES;
    //i1.layer.borderWidth = 1;
    
    i2.layer.cornerRadius=50/2;
    i2.layer.masksToBounds=YES;
    
    i3.layer.cornerRadius=50/2;
    i3.layer.masksToBounds=YES;
    
    i5.layer.cornerRadius=50/2;
    i5.layer.masksToBounds=YES;
    
    i6.layer.cornerRadius=50/2;
    i6.layer.masksToBounds=YES;
    
    demo.layer.cornerRadius=50/2;
    demo.layer.masksToBounds=YES;
    
    video1.layer.cornerRadius=50/2;
    video1.layer.masksToBounds=YES;
    
    video2.layer.cornerRadius=50/2;
    video2.layer.masksToBounds=YES;
    
    video3.layer.cornerRadius=50/2;
    video3.layer.masksToBounds=YES;
    
    video4.layer.cornerRadius=50/2;
    video4.layer.masksToBounds=YES;
    
    video5.layer.cornerRadius=50/2;
    video5.layer.masksToBounds=YES;
    
    video6.layer.cornerRadius=50/2;
    video6.layer.masksToBounds=YES;
    
    
    v.layer.cornerRadius=30/2;
    v.layer.masksToBounds=YES;
    
    i.layer.cornerRadius=30/2;
    i.layer.masksToBounds=YES;
    
    d.layer.cornerRadius=30/2;
    d.layer.masksToBounds=YES;
    
    e.layer.cornerRadius=30/2;
    e.layer.masksToBounds=YES;
    
    o.layer.cornerRadius=30/2;
    o.layer.masksToBounds=YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)video1:(id)sender {
    buttontag = 1;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}

- (IBAction)video2:(id)sender {
    buttontag= 2;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}

- (IBAction)video3:(id)sender {
    buttontag = 3;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}

- (IBAction)video4:(id)sender {
    buttontag = 4;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}

- (IBAction)video5:(id)sender {
    buttontag = 5;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}
- (IBAction)demo:(id)sender {
    buttontag = 6;
    [self performSegueWithIdentifier:@"pass" sender:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ViewController *vc = [segue destinationViewController];
    
    // Pass any objects to the view controller here, like...
    vc.tag=buttontag;
}


@end
