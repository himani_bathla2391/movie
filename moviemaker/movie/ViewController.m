//
//  ViewController.m
//  movie
//
//  Created by Clicklabs 104 on 10/9/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIImageView *bg;
@property (weak, nonatomic) IBOutlet UIButton *pause;

@property (weak, nonatomic) IBOutlet UIButton *choose;


@end

@implementation ViewController
@synthesize image;
@synthesize bg;
@synthesize play;
@synthesize choose;
@synthesize tag;
@synthesize pause;
UIImage *img1;
UIImage *img2;
UIImage *img3;
UIImage *img4;
UIImage *img5;
UIImage *img6;
UIImage *img7;
UIImage *img8;
UIImage *img9;
UIImage *img10;
UIImage *img11;
UIImage *img12;
UIImage *img13;
UIImage *img14;
UIImage *img15;
UIImage *img16;
UIImage *img17;
UIImage *img;
UIImagePickerController *imagePickerController;
AVAudioPlayer *audioPlayer;

NSArray *array1;

UIImage *img18;
UIImage *img19;
UIImage *img20;
UIImage *img21;
UIImage *img22;
UIImage *img23;
UIImage *img24;
UIImage *img25;
UIImage *img26;
UIImage *img27;

UIImage *c1;
UIImage *c2;
UIImage *c3;
UIImage *c4;
UIImage *c5;
UIImage *c6;
UIImage *c7;

UIImage *ic1;
UIImage *ic2;
UIImage *ic3;
UIImage *ic4;
UIImage *ic5;
UIImage *ic6;
UIImage *ic7;
UIImage *ic8;
UIImage *ic9;
UIImage *ic10;
UIImage *ic11;
UIImage *ic12;
UIImage *ic13;
UIImage *ic14;
UIImage *ic15;
UIImage *ic16;
UIImage *ic17;
UIImage *ic18;
UIImage *ic19;
UIImage *ic20;
UIImage *ic21;
UIImage *ic22;
UIImage *ic23;
UIImage *ic24;
UIImage *ic25;
UIImage *ic26;
UIImage *ic27;
UIImage *ic28;
UIImage *ic29;
UIImage *ic30;
UIImage *ic31;
UIImage *ic32;
UIImage *ic33;
UIImage *ic34;
UIImage *ic35;
UIImage *ic36;
UIImage *ic37;
UIImage *ic38;
UIImage *ic39;
UIImage *ic;


UIImage *d1;
UIImage *d2;
UIImage *d3;
UIImage *d4;
UIImage *d5;
UIImage *d6;
UIImage *d7;
UIImage *d8;
UIImage *d9;
UIImage *d10;
UIImage *d11;
UIImage *d12;
UIImage *d13;

UIImage *f1;
UIImage *f2;
UIImage *f3;
UIImage *f4;
UIImage *f5;
UIImage *f6;
UIImage *f7;
UIImage *f8;
UIImage *f9;
UIImage *f10;
UIImage *f11;

//MPMoviePlayerViewController *moviePlayer;
- (void)viewDidLoad {
    [super viewDidLoad];
    img1= [UIImage imageNamed:@"1.jpg"];
    img2= [UIImage imageNamed:@"2.jpg"];
    img3= [UIImage imageNamed:@"3.jpg"];
    img4= [UIImage imageNamed:@"4.jpg"];
    img5= [UIImage imageNamed:@"5.jpg"];
    img6= [UIImage imageNamed:@"6.jpg"];
    img7= [UIImage imageNamed:@"7.jpg"];
    img8= [UIImage imageNamed:@"8.jpg"];
    img9= [UIImage imageNamed:@"9.jpg"];
    img10= [UIImage imageNamed:@"10.jpg"];
    img11= [UIImage imageNamed:@"11.jpg"];
    img12= [UIImage imageNamed:@"12.jpg"];
    img13= [UIImage imageNamed:@"13.jpg"];
    img14= [UIImage imageNamed:@"14.jpg"];
    img15= [UIImage imageNamed:@"15.jpg"];
    img16= [UIImage imageNamed:@"16.jpg"];
    img17= [UIImage imageNamed:@"17.jpg"];
    img= [UIImage imageNamed:@"imgres-6.jpg"];
    
    img18= [UIImage imageNamed:@"18.jpg"];
    img19= [UIImage imageNamed:@"19.jpg"];
    img20= [UIImage imageNamed:@"20.jpg"];
    img21= [UIImage imageNamed:@"21.jpg"];
    img22= [UIImage imageNamed:@"22.jpg"];
    img23= [UIImage imageNamed:@"23.jpg"];
    img24= [UIImage imageNamed:@"24.jpg"];
    img25= [UIImage imageNamed:@"25.jpg"];
    img26= [UIImage imageNamed:@"26.jpg"];
    img27= [UIImage imageNamed:@"27.jpg"];

    c1=[UIImage imageNamed:@"cli4.png"];
    c2=[UIImage imageNamed:@"cli.png"];
    c3=[UIImage imageNamed:@"cli1.png"];
    c4=[UIImage imageNamed:@"cli2.png"];
    c5=[UIImage imageNamed:@"cli5.png"];
    c6=[UIImage imageNamed:@"cli6.png"];
    c7=[UIImage imageNamed:@"cli7.png"];
    
    ic1=[UIImage imageNamed:@"ic1.jpg"];
    ic2=[UIImage imageNamed:@"ic2.jpg"];
    ic3=[UIImage imageNamed:@"ic3.jpg"];
    ic4=[UIImage imageNamed:@"ic4.jpg"];
    ic5=[UIImage imageNamed:@"ic5.jpg"];
    ic6=[UIImage imageNamed:@"ic6.jpg"];
    ic7=[UIImage imageNamed:@"ic7.jpg"];
    ic8=[UIImage imageNamed:@"ic8.jpg"];
    ic9=[UIImage imageNamed:@"ic9.jpg"];
    ic10=[UIImage imageNamed:@"ic10.jpg"];
    ic11=[UIImage imageNamed:@"ic11.jpg"];
    ic12=[UIImage imageNamed:@"ic12.jpg"];
    ic13=[UIImage imageNamed:@"ic13.jpg"];
    ic=[UIImage imageNamed:@"ic.jpg"];
    ic14=[UIImage imageNamed:@"ic14.jpg"];
    ic15=[UIImage imageNamed:@"ic15.jpg"];
    ic16=[UIImage imageNamed:@"ic16.jpg"];
    ic17=[UIImage imageNamed:@"ic17.jpg"];
    ic18=[UIImage imageNamed:@"ic18.jpg"];
    ic19=[UIImage imageNamed:@"ic19.jpg"];
    ic20=[UIImage imageNamed:@"ic20.jpg"];
    ic21=[UIImage imageNamed:@"ic21.jpg"];
    ic22=[UIImage imageNamed:@"ic22.jpg"];
    ic23=[UIImage imageNamed:@"ic23.jpg"];
    ic24=[UIImage imageNamed:@"ic24.jpg"];
    ic25=[UIImage imageNamed:@"ic25.jpg"];
    ic26=[UIImage imageNamed:@"ic26.jpg"];
    ic27=[UIImage imageNamed:@"ic27.jpg"];
    ic28=[UIImage imageNamed:@"ic28.jpg"];
    ic29=[UIImage imageNamed:@"ic29.jpg"];
    ic30=[UIImage imageNamed:@"ic30.jpg"];
    ic31=[UIImage imageNamed:@"ic31.jpg"];
    ic32=[UIImage imageNamed:@"ic32.jpg"];
    ic33=[UIImage imageNamed:@"ic33.jpg"];
    ic34=[UIImage imageNamed:@"ic34.jpg"];
    ic35=[UIImage imageNamed:@"ic35.jpg"];
    ic36=[UIImage imageNamed:@"ic36.jpg"];
    ic37=[UIImage imageNamed:@"ic37.jpg"];
    ic38=[UIImage imageNamed:@"ic38.jpg"];

    
    d1= [UIImage imageNamed:@"d1.jpg"];
    d2= [UIImage imageNamed:@"d2.jpg"];
    d3= [UIImage imageNamed:@"d3.jpg"];
    d4= [UIImage imageNamed:@"d4.jpg"];
    d5= [UIImage imageNamed:@"d5.jpg"];
    d6= [UIImage imageNamed:@"d6.jpg"];
    d7= [UIImage imageNamed:@"d7.jpg"];
    d8= [UIImage imageNamed:@"d8.jpg"];
    d9= [UIImage imageNamed:@"d9.jpg"];
    d10= [UIImage imageNamed:@"d10.jpg"];
    d11= [UIImage imageNamed:@"d11.jpg"];
    d12= [UIImage imageNamed:@"d12.jpg"];
    d13= [UIImage imageNamed:@"d13.jpg"];
    
    f1= [UIImage imageNamed:@"f1.jpg"];
    f2= [UIImage imageNamed:@"f2.jpg"];
    f3= [UIImage imageNamed:@"f3.jpg"];
    f4= [UIImage imageNamed:@"f4.jpg"];
    f5= [UIImage imageNamed:@"f5.jpg"];
    f6= [UIImage imageNamed:@"f6.jpg"];
    f7= [UIImage imageNamed:@"f7.jpg"];
    f8= [UIImage imageNamed:@"f8.jpg"];
    f9= [UIImage imageNamed:@"f9.jpg"];
    f10= [UIImage imageNamed:@"f10.jpg"];
    f11= [UIImage imageNamed:@"f11.jpg"];
    
    play.layer.cornerRadius=50/2;
    play.layer.masksToBounds=YES;
    play.layer.borderWidth=1.0;
    
    pause.layer.cornerRadius=50/2;
    pause.layer.masksToBounds=YES;
    
    //NSArray arrayWithObjects: img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12,img13,img14,img15,img16,img17,nil];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)choose:(id)sender {
    imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    //imagePickerController.mediaTypes = [NSArray arrayWithObject:(NSString *)image];
    
//    popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
//    [popoverController presentPopoverFromRect:CGRectMake(0.0, 0.0, 400.0, 300.0)
//                                       inView:self.view
//                     permittedArrowDirections:UIPopoverArrowDirectionAny
//                                     animated:YES];
    
    [self presentModalViewController:imagePickerController animated:YES];
    
}
- (IBAction)pause:(id)sender {
    
    [audioPlayer pause];
    [image stopAnimating];
    
}


- (IBAction)play:(id)sender {
    if (tag ==1) {
        image.animationImages = [NSArray arrayWithObjects: img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12,img13,img14,img15,img16,img17,nil];
        image.animationDuration = 35.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"agnee" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        //audioPlayer.duration=NSTimeInterval theTimeInterval = 35.0;
        
        [audioPlayer play];
    }
    
    else if (tag ==2){
        image.animationImages = [NSArray arrayWithObjects: nil];
        image.animationImages = [NSArray arrayWithObjects: img18,img19,img20,img21,img22,img23,img24,img25,img26,img27,nil];
        image.animationDuration = 35.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"give_me_some_sunshin" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
    }
    
    else if (tag ==3){
        image.animationImages = [NSArray arrayWithObjects: c1,c2,c3,c4,c5,c6,c7,nil];
        image.animationDuration = 20.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"rubaroo_guita" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
    }
    else if (tag ==4){
        image.animationImages = [NSArray arrayWithObjects: ic1,ic2,ic3,ic4,ic5,ic6,ic7,ic8,ic9,ic10,ic11,ic12,ic13,ic,ic14,ic15,ic16,ic17,ic18,ic19,ic20,ic21,ic22,ic23,ic24,ic25,ic26,ic27,ic28,ic29,ic30,ic31,ic32,ic33,ic34,ic35,ic36,ic37,ic38,nil];
        image.animationDuration = 35.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"aesadesh" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
    }
    else if (tag ==5){
        image.animationImages = [NSArray arrayWithObjects: d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,nil];
        image.animationDuration = 35.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"dance_dance" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
    }
    else if (tag ==6){
        image.animationImages = [NSArray arrayWithObjects: f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,nil];
        image.animationDuration = 31.0;
        image.animationRepeatCount=1;
        [image startAnimating];
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"new_yaro_dosti_remix" ofType:@"mp3"];
        audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:
                       [NSURL fileURLWithPath:path] error:NULL];
        [audioPlayer play];
    }



}
//    image.image = image.animationImages.lastObject;

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
    [[picker parentViewController]dismissModalViewControllerAnimated:YES];
   bg.image=img;
// image.animationImages= img;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
